import './App.css';
import Header from './components/header/header';
import ContentPage from './content-page/contentPage';

function App() {
  return (
    <div className="App">
        <Header/>
        <ContentPage/>
    </div>
  );
}

export default App;
