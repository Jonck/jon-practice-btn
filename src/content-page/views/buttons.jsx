import React from 'react';
import Btn from '../../components/general/btn';
import './css/buttons.css';


const tipeBtn = [
    [
        {alternativeText: '<Button />', dataClass: 'default', DataText: ''}, 
        {alternativeText: '&:hover, &:focus', dataClass: 'default active', DataText: ''}
    ],
    [
        {alternativeText: '<Button variant=”outline” />', dataClass: 'outline', DataText: ''}, 
        {alternativeText: '&:hover, &:focus', dataClass: 'outline active', DataText: ''}
    ],
    [
        {alternativeText: '<Button variant=”text” />', dataClass: 'text', DataText: ''}, 
        {alternativeText: '&:hover, &:focus', dataClass: 'text active', DataText: ''}
    ],
    [
        {alternativeText: '<Button disableShadow />', dataClass: 'primary', DataText: ''}
    ],
    [
        {alternativeText: '<Button disabled />', dataClass: 'disable', DataText: ''}, 
        {alternativeText: '<Button variant=”text” disabled />', dataClass: 'disable-text', DataText: ''}
    ],
    [
        {alternativeText: '<Button startIcon=”local_grocery_store” />', dataClass: 'primary store', DataText: ''},
        {alternativeText: '<Button endIcon=”local_grocery_store” />', dataClass: 'primary store invert', DataText: ''}
    ],
    [
        {alternativeText: '<Button size=”sm” />', dataClass: 'primary', DataText: ''},
        {alternativeText: '<Button size=”md” />', dataClass: 'primary', DataText: ''}, 
        {alternativeText: '<Button size=”lg” />', dataClass: 'primary', DataText: ''}, 
    ],
    [
        {alternativeText: '<Button color=”default” />', dataClass: 'default', DataText: ''}, 
        {alternativeText: '<Button color=”primary” />', dataClass: 'primary', DataText: ''},
        {alternativeText: '<Button color=”secondary” />', dataClass: 'secondary', DataText: ''},
        {alternativeText: '<Button color=”danger” />', dataClass: 'danger', DataText: ''}
    ],
    [
        {alternativeText: '<Button color=”default” />', dataClass: 'default active', DataText: ''}, 
        {alternativeText: '<Button color=”primary” />', dataClass: 'primary active', DataText: ''},
        {alternativeText: '<Button color=”secondary” />', dataClass: 'secondary active', DataText: ''},
        {alternativeText: '<Button color=”danger” />', dataClass: 'danger active', DataText: ''}
    ],   
];

const Buttons = props => {
    return (
        <div className="buttons">
            {tipeBtn.map((el, index) => {
                return (
                    <div className="buttons__componet" key={`componentBtn--${index}`}>
                        {el.map((elem, i) => {
                            return (
                                <div className="itemBtn">
                                    <p className="itemBtn__text">{elem.alternativeText}</p>
                                    <Btn info={elem} key={'btn--'+i}/>
                                </div>
                            )
                        })}
                    </div>
                )
            })}
        </div>
    );
}

export default Buttons;
