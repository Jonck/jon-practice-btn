import React, { useEffect, useState}from 'react';
import Buttons from './views/buttons';
import DefaultContent from './views/default';

const ContentPage = props => {
    const [ContentPageData, setData] = useState('default');

    useEffect((dataPrint= 'buttons') => {
        setData(dataPrint);
        console.log('el efecto se ejecuto bien');
    }, []);

    const renderData = () => {
        let res;
        switch (ContentPageData) {
            case 'buttons':
               res = <Buttons /> 
            break;
            default:
               res = <DefaultContent /> 
            break;
        }
        return res;
    }

    return (
        <div className={'page-'+ ContentPageData}>
            {renderData()}
        </div>
    )
}

export default ContentPage;
