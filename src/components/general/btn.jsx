import React, { Fragment } from 'react'
import "./css/btn.css"

const Btn = props => {
    const {dataClass, DataText} = props.info;
    return (
        <Fragment>
            <button className={'btn btn--'+dataClass}>{DataText !== '' ? DataText : 'Default'}</button>
        </Fragment>
    );
}

export default Btn;
