import React, {Fragment} from 'react'

const RenderImage = props => {
    const {imgSrc} = props;

    console.log(imgSrc);
    
    return (
        <Fragment>
            <img src={imgSrc} alt="logo"/>
        </Fragment>
    );
}

export default RenderImage;