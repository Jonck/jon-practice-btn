import React, { Fragment } from 'react';
const links = ['Colors', 'Typography', 'Spaces', 'Buttons', 'Input', 'Grid'];

const Navigation = props => {
    return (
        <Fragment>
            {links.map((link, index) => <a key={index } href={`/${link.toLowerCase()}`}>{link}</a>)}
        </Fragment>
    )
}


export default Navigation;