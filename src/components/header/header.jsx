import React, {Fragment} from 'react';
import DevChallenges from '../logos/logo';
import Navigation from './navigation';
import './css/header.css'

const openHeader = e => {
    e.target.parentNode.classList.toggle('active')
}


const Header = props => {
    return (
        <Fragment>
            <header className="header">
                <div className="headerBTN" onClick={openHeader} ></div>
                <div className="header__logo">
                    <DevChallenges/>
                </div>
                <nav className="header__nav">
                    <Navigation />
                </nav>
            </header>
        </Fragment>
    )
}

export default Header;
